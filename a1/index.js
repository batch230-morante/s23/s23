/*
s23 Activity Instructions:
1. In the S23 folder, create an a1 folder and an index.html and script.js file inside of it.
2. Link the script.js file to the index.html file.
3. Create a trainer object using object literals.
4. Initialize/add the following trainer object properties:
a. Name (String)
b. Age (Number)
c. Pokemon (Array)
d. Friends (Object with Array values for properties)
5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
6. Access the trainer object properties using dot and square bracket notation.
7. Invoke/call the trainer talk object method.
8. Create a git repository named S23.
9. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
10. Add the link in Boodle.
*/

let trainer = {
    name: "Ash Ketchum",
    age: 18,
    pokeMon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    friends: {
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"]
    },
    talk: function(){
        console.log("Pikachu! I choose you!")
    }
}
console.log(trainer);
console.log("Result of dot notation: ");
console.log(trainer.name);
console.log("Result of square bracket notation: ");
console.log(trainer["pokeMon"]);
console.log("Result of talk method: ");
trainer.talk();

function Pokemon(name, level, health, attack){
    this.name = name;
    this.level = level;
    this.health = health;
    this.attack = attack;
// ----------------------------------
    this.tackle = function(target){
        console.log(this.name + " tackled " + target.name);
        target.health -= this.attack;
        console.log(target.name + "'s health is now reduced to " + target.health)
        if(target.health <= 0){
            console.log(target.name + " fainted");
        }
    }

}

let pokemon1 = new Pokemon("Pikachu", 12, 24, 12);
let pokemon2 = new Pokemon("Geodude", 8, 16, 8);
let pokemon3 = new Pokemon("Mewtwo", 100, 200, 100);

console.log(pokemon1);
console.log(pokemon2);
console.log(pokemon3);

pokemon2.tackle(pokemon1);

console.log(pokemon1);

pokemon3.tackle(pokemon2);

console.log(pokemon2);


// safsdffsdgsdf