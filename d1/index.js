// [SECTION] Objects
/*
    An object is a data type that is used to represent real world objects. 
    It is a collection of related data and/or functionalities. 
    Information is stored in object represented in key:value pair
        // key -> property of the object
        // value -> actual data to be stored

    two ways to create an object in javascript
    1. Object literal notation (let object = {})
    2. Object constructor notation 
        // object instantiation (let object = newObject();)
*/

    // Object name
let cellphone = {
    name: "Nokia 3210",
    manufactureDate: 1999
};

console.log("Result from creating objects using initializers/ literal notation: ");
console.log(cellphone);
console.log(typeof cellphone);

/*
    name is a property or key with value "Nokia 3210"
    manufactureDate is a property or key with value "1999"
*/

let cellphone2 = {
    name: "iPhone 13",
    manufactureDate: 2021
};

console.log(cellphone2);

/*
    Create an object with a name cellphone3,
    with property name and value "Xiaomi 11t Pro"
    and another property "manufactureDate" and value "2021"
*/

let cellphone3 = {
    name: "Xiaomi 11t Pro",
    manufactureDate: 2021
};
console.log(cellphone3);

// Object Construction Notation
/*
    creating objects using a constructor function
        creates a usable function to create several objects that have 
        the same data structure (blueprint)

    syntax:
        function objectName(keyA, keyB){
            this.keyA = keyA;
            this.keyB = keyB;
        }

    // "this" keyword refers to the properties within the object
    // it allows the assignment of new object's properties by associating
       them with values received from the constructor function's parameter
*/


function Laptop(name, manufactureDate){
    this.name = name;
    this.manufactureDate = manufactureDate;
}

let laptop = new Laptop("Lenovo", 2008);

console.log(laptop);


let laptop2 = new Laptop("Macbook Air", 2008);
console.log(laptop2);


let laptop3 = new Laptop("Portal R2E CCMC", 1980);
console.log(laptop3);

// Create an empty object
let computer = {};
let myComputer = new Object();


// [SECTION] Accessing object properties / Keys

// using dot notation 
// syntax: objectName.preopertyName
console.log("Result from dot notation: " + laptop2.name);


// using square bracket notation
console.log("Result from square bracket notation: " + laptop2["name"]);


// Accessing array of objects
// Accessing object properties using the square bracket notation and array indexes can cause confusion

let arrayObj = [laptop, laptop2]
// may be confused if you are accessing array indexes
console.log(arrayObj[0]["name"]);

// this tells us that array[0] is an object using the dot notation
console.log(arrayObj[0].name);

// [SECTION] Initializing / Adding / Deleting / Reassigning Object properties
let car = {};
console.log("Current value of car object: ");
console.log(car);

// initializing/adding object properties
car.name = "Honda Civic";
console.log("Result from adding properties using dot notation");
console.log(car);

// Initializing/adding object using square bracket notation [not recommended]
car["manufacture date"] = 2019;
console.log("Result from adding properties using square bracket notation");
console.log(car);

// Deleting object properties
delete car["manufacture date"];
console.log("Result from deleting properties: ");
console.log(car);

// Reassigning object property values
car.name = "Toyota Vios"
console.log("Result from reassinging property values: ");
console.log(car);


// [SECTION] Object methods
    // a method is a function which is a property of an object

let person = {
    name: "John",
    talk: function(){
        console.log("Hello my name is " + this.name);
    }
}


person.walk = function(steps){
    console.log(this.name + " walked " + steps + " steps forward");
}

console.log(person);
person.walk(50);


console.log(person);
// syntax: objectName.preopertyName
person.talk();


let friend = {
    firstName: "Joe",
    lastName: "Smith",
    address:{
        city: "Austin",
        country: "Texas"
    },
    emails: ["joe@mail.com", "joemsmith@mail.xyz"],
    introduce: function(){
        console.log("Hello my name is " + this.firstName + " " + this.lastName + ". " + "I live in " + this.address.city + ", " + this.address.country);
    }
}

friend.introduce();


// [SECTION] Real World Application of Objects
/*
	Scenario:
	1. We would like to create a game that would have a several pokemon interact with each other.
	2. Every pokemon would have the same set of stats, properties and function.

	Stats:
	name
	level
	health = level * 2
	attack = level

*/

// create an object constructor to lessen the process in creating the pokemon
function pokeMon(name, level){
    // properties
    this.name = name;
    this.level = level;
    this.health = level * 2;
    this.attack = level;
    
// method
// "target" parameter represents another pokemon object
    this.tackle = function(target){
        console.log(this.name + " tackled " + target.name);
        target.health -= this.attack;
        console.log("targetPokemon's health is now reduced to " + target.health);
    }
    this.faint = function(){
        console.log(this.name + " fainted");
    }
    if(this.health <= 0){
        console.log(target + " has fainted.");
    }


}

let pikachu = new pokeMon("Pikachu", 99);
console.log(pikachu);

let rattata = new pokeMon("Rattata", 5);
console.log(rattata);


pikachu.tackle(rattata);
console.log();